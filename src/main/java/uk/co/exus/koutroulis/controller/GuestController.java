package uk.co.exus.koutroulis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by ch.koutroulis on 05-Jul-17.
 */

@Controller
public class GuestController {

    @GetMapping(path = "/guest")
    public String getGuestPage() {
        return "guest-page";
    }
}
